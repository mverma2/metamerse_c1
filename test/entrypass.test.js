const {
    BN, // Big Number support
    constants, // Common constants, like the zero address and largest integers
    expectEvent, // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require("@openzeppelin/test-helpers");

const { fromWei, toWei } = require("web3-utils");

const EntryPass = artifacts.require("EntryPass");


contract("EntryPass", (accounts) => {
    let entrypassInstance = null;
    const [owner, user, bob, steve, blackListUser, george] = accounts;    

    airDropUser = Array(user, bob);
    airDropVal = Array(1, 2);

    const eventname = {
        Transfer: "TransferSingle",
        newNFTCreated: "newNFTCreated",
        newAirDropToken: "newAirDropToken",
        Paused: "Paused",
        Unpaused: "Unpaused",
        setNFTValue: "setNFTValue",
        setUri: "setUri"

    };

    const id = 0;
    const mintNftVal = 1;
    const name = "Entry Pass Contract";
    const symbol = "EPC";
    let mintPrice = toWei("0.1", "ether");
    const baseUri = "https://www.google.com/";

    before("Deploy new Entry Pass contract", async () => {
        await EntryPass.new(name, symbol, { from: owner }).then(
            (instance) => (entrypassInstance = instance)
        );
    });

    describe("New Entry Pass Contract", async() => {
        it('has a name', async function () {
            expect(await entrypassInstance.name()).to.equal(name);
        });

        it('has a symbol', async function () {
            expect(await entrypassInstance.symbol()).to.equal(symbol);
        });
    });

    describe("Set NFT Price", async() => {
        describe("When owner set the NFT Price", async() => {
            it('should set the price', async function() {
                const unpauseContract = await entrypassInstance.setNFTPrice(0, mintPrice,
                    {
                        from: owner,
                    }
                );
                await expectEvent(unpauseContract, eventname.setNFTValue, {
                    _mintValue : mintPrice
                });
            });
        });
    });
    
    describe("Set Uri Value", async() => {
        describe("When owner set the Uri Value", async() => {
            it('should set the uri value', async function() {
                const unpauseContract = await entrypassInstance.setURI(0, baseUri,
                    {
                        from: owner,
                    }
                );
                await expectEvent(unpauseContract, eventname.setUri, {
                    _seturi : baseUri
                });
            });
        });
    });

    describe("Mint Nft", async() => {
        describe("When user mint a NFT", async() => {
            it('should mint token', async function() {
                const mintNftReceipt = await entrypassInstance.mint(user, id, mintNftVal,
                    {
                        from: user,
                        value: mintPrice.toString()              
                    }
                );
                await expectEvent(mintNftReceipt, eventname.Transfer, {
                    operator: user,
                    from : constants.ZERO_ADDRESS,
                    to : user,
                    id : id.toString(),
                    value: mintNftVal.toString()
                });
    
                await expectEvent(mintNftReceipt, eventname.newNFTCreated, {
                    _id : id.toString()
                });    
            });            
        });

        describe("When price is low", async() => {
            it('should not mint this token', async function() {
                await expectRevert(
                    entrypassInstance.mint(user, id, mintNftVal,
                        {
                            from: user,
                            value: 0              
                        }
                    ),
                    "Not enough ETH to Mint the NFT"
                );
            });            
        })
    });

    describe("Airdrop", async() => {
        describe("When owner tries to airdrop", async() => {
            it('should airdrop token', async function() {
                const airdropReceipt = await entrypassInstance.airDrop(airDropUser, id, airDropVal, {
                    from: owner,
                });
                await expectEvent(airdropReceipt, eventname.Transfer, {
                    operator: owner,
                    from : constants.ZERO_ADDRESS,
                    to : user,
                    id : id.toString(),
                    value: mintNftVal.toString()
                });

                await expectEvent(airdropReceipt, eventname.newAirDropToken, {
                    _id : id.toString()
                });
            });
        })

        describe("When other user tries to airdrop", async() => {
            it('should not airdrop token', async function() {                
                await expectRevert(
                    entrypassInstance.airDrop(airDropUser, id, airDropVal, {
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        })
    });

    describe("Pause Contract", async() => {
        describe("When owner pause a contract", async() => {
            it('should pause the contract', async function() {
                const pauseContract = await entrypassInstance.pause(
                    {
                        from: owner,
                    }
                );
                await expectEvent(pauseContract, eventname.Paused, {
                    account : owner
                });
            });
        });

        describe("When other user pause a contract", async() => {
            it('should not pause the contract', async function() {
                await expectRevert(
                    entrypassInstance.pause(
                        {
                            from: user,
                        }
                    ),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });


    describe("Unpause Contract", async() => {
        describe("When owner unpause a contract", async() => {
            it('should unpause the contract', async function() {
                const unpauseContract = await entrypassInstance.unpause(
                    {
                        from: owner,
                    }
                );
                await expectEvent(unpauseContract, eventname.Unpaused, {
                    account : owner
                });
            });
        });

        describe("When other user unpause a contract", async() => {
            it('should not unpause the contract', async function() {
                await expectRevert(
                    entrypassInstance.unpause(
                        {
                            from: user,
                        }
                    ),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });

    
});