// SPDX-License-Identifier: MIT

pragma solidity ^0.8.4;

/**
 * @dev Interface of the Entrypas token implementation.
 */

interface IEntryPass { 

    event newNFTCreated(uint256 _id);

    event newAirDropToken(uint256 _id);

    event setNFTValue(uint256 _mintValue);

    event setUri(string _seturi);

    function setURI(uint _id, string memory _seturi) external;

    function setNFTPrice(uint _id, uint256 _mintValue) external;

    function _uri(uint _id) external view returns (string memory); 

    function mint(address account, uint256 _id, uint256 amount) external payable;   

    function airDrop(address[] memory recipient, uint256 _id, uint256[] memory amount) external;

    function pause() external;

    function unpause() external;
}