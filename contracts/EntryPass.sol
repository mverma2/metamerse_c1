// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "./IEntryPass.sol";

contract EntryPass is ERC1155, Ownable, Pausable, IEntryPass {
    
    string public name;

    string public symbol;

    mapping(uint256 => bool) public airDroppedTokens;

    mapping(uint => string) public tokenURI;

    mapping(uint => uint256) public mintPrice;
    
    constructor(
        string memory _name, 
        string memory _symbol
        ) 
        ERC1155("") 
    {
        name = _name;
        symbol = _symbol;
    }

    function setNFTPrice(uint _id ,uint256 _mintValue) 
        external 
        override 
        onlyOwner 
    {
        mintPrice[_id] = _mintValue;
        
        emit setNFTValue(_mintValue);

    }

    function pause() 
        external 
        override 
        onlyOwner 
    {
        _pause();
    }

    function unpause() 
        external 
        override 
        onlyOwner 
    {
        _unpause();
    }

    function mint(
        address account, 
        uint256 _id, 
        uint256 amount
        )
        external 
        override 
        payable
    {
        require(msg.value >= mintPrice[_id], "Not enough ETH to Mint the NFT; Please check price!");
        _mint(account, _id, amount, "");
        emit newNFTCreated(_id);
    }

    function airDrop(
        address[] memory recipient, 
        uint256 _id, uint256[] memory amount
        ) 
        external
        override  
        onlyOwner 
        whenNotPaused 
    {
        require(amount.length == recipient.length, "Incorrect parameter length");
        for (uint256 index = 0; index < recipient.length; index++) {
            _mint(recipient[index], _id, amount[index], "");
            airDroppedTokens[index] = true;
        }
        emit newAirDropToken(_id);
    }

    function setURI(
        uint _id, 
        string memory _seturi
        ) 
        external
        override 
        onlyOwner 
    {
        tokenURI[_id] = _seturi;
        emit setUri(_seturi);
    }

    function _uri(
        uint _id
        ) 
        external 
        view 
        override 
        returns (string memory) 
    {
        return tokenURI[_id];
    }

    function _beforeTokenTransfer(
        address operator, 
        address from, 
        address to, 
        uint256[] memory ids, 
        uint256[] memory amounts, 
        bytes memory data
        )
        internal
        whenNotPaused
        override
    {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }
}